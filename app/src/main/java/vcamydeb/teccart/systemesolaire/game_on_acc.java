package vcamydeb.teccart.systemesolaire;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;


public class game_on_acc extends AppCompatActivity {

    private AlienSolarSystemAcc AlienSolarSystemAcc;
    public int height;
    public int width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;


        AlienSolarSystemAcc = new AlienSolarSystemAcc(this, height, width);
        setContentView(AlienSolarSystemAcc);


    }
}

