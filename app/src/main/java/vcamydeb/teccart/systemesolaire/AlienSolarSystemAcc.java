package vcamydeb.teccart.systemesolaire;
import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AlienSolarSystemAcc extends View implements SensorEventListener{

    private Random alea;
    private int ballX;
    private int bally;
    private planetes[] planetes = new planetes[8];
    private int cnt;
    private Context mcontext;
    private boolean fin;
    private String nom;
    private int taille;
    private String couleur;
    private int type;
    private String nomImage;
    private Bitmap space;
    private int screenW;
    private int screenH;
    private ArrayList<AstreCeleste> liste;
    private Bitmap imageSelect;
    private Bitmap resizedImage;
    private Bitmap resizedSpace;
    private VaisseauSpatial vaisseau;
    public float xvaisseau;
    public float yvaisseau;
    private SensorManager sm ;
    private List<Sensor> sensorList;
    private Sensor accelerometer;
    public int Maxwidth;
    public int Maxheight;

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
    }

    public AlienSolarSystemAcc(Context context, int height, int width) {
        super(context);
        mcontext = context;

        Maxwidth = width;
        Maxheight = height;


        myDbAdapter myDBAdapter = new myDbAdapter(mcontext);
        myDBAdapter.Open();
        liste = myDBAdapter.selectAstres();

        fin = false;
        cnt = 0;
        alea = new Random();

        ballX = alea.nextInt(Maxwidth);
        bally = alea.nextInt(Maxheight);




        //sensor////////////////////////////////////
        sm = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensorList = sm.getSensorList(Sensor.TYPE_ALL);

        for(Sensor element:sensorList) {
            System.out.println("Sensor vendor:" + element.toString());
            System.out.println("");
        }

        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        //////////////////////////////////////////


        for (int i = 0; i <= 7; i++) {

            nom = liste.get(i).getNomAstre();
            taille = liste.get(i).getTailleAstre();
            couleur = liste.get(i).getCouleurAstre();
            type = liste.get(i).isStatusAstre();
            nomImage = liste.get(i).getNomImageAstre();

            planetes temp = new planetes(mcontext,nom, taille, couleur, type, nomImage, height, width );
            planetes[i] = temp;

        }

        if (cnt >= 7 && !fin) {
            Toast.makeText(mcontext, "La partie est terminee", Toast.LENGTH_LONG).show();
            fin = true;
        }

        //creer le vaisseau
        try{
            vaisseau = new VaisseauSpatial("vaisseau", 400, 320);

            int idImage = getResources().getIdentifier(vaisseau.getNomImage(), "drawable", getContext().getPackageName());
            imageSelect = BitmapFactory.decodeResource(getResources(), idImage);
            resizedImage = Bitmap.createScaledBitmap(imageSelect, vaisseau.getWidth(), vaisseau.getHeight(), true);

            //        //creer le background
            space = BitmapFactory.decodeResource(getResources(), R.drawable.space);
            resizedSpace = Bitmap.createScaledBitmap(space, width , height, true);
        }



        catch(Exception ex){
            String test = ex.getMessage();
        }


    }


    @Override
    protected void onDraw(Canvas canvas) {



        canvas.drawBitmap(resizedSpace, 0, 0, null);


        canvas.drawBitmap(resizedImage, ballX , bally, null);


        //creer les planetes
        for (int i = 0; i <= 7; i++) {

            planetes[i].onDraw(canvas);
        }


        if (cnt >= 7 && !fin) {
            Toast.makeText(mcontext, "La partie est terminee", Toast.LENGTH_LONG).show();
            fin = true;
        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float vectorLength;

        vectorLength = (float)Math.sqrt(Math.pow((double)(event.values[0]),2)+Math.pow((double)(event.values[1]),2)
                + Math.pow((double)(event.values[2]),2));

        ballX = (int)(ballX + event.values[0] *(-1 * vectorLength));
        bally = (int)(bally + event.values[1] * vectorLength);




        boolean limitL, limitR, LimitU, LimitD = false;


        for (int i = 0; i < planetes.length; i++) {



            nom = liste.get(i).getNomAstre();
            taille = liste.get(i).getTailleAstre();
            couleur = liste.get(i).getCouleurAstre();
            type = liste.get(i).isStatusAstre();
            nomImage = liste.get(i).getNomImageAstre();


            limitL = ballX > (planetes[i].getPosX() - taille);
            limitR = ballX < (planetes[i].getPosX() + taille);
            LimitU = bally > (planetes[i].getPosY() - taille);
            LimitD = bally < (planetes[i].getPosY() + taille);

            if (limitL && limitR && LimitD && LimitU) {


                if (planetes[i].getStatus()) {


                    planetes[i].setStatus(false, nom, couleur, taille, type);


                    cnt++;
                }


                System.out.println(cnt);

            }

        }

        invalidate();
    }





    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}


