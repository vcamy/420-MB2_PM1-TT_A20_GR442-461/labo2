package vcamydeb.teccart.systemesolaire;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class planetes extends View {

    private int posX;
    private int posY;
    private Random alea;
    private boolean status;
    private Paint crayon;
    private Color pinceau;
    private String image;
    private Bitmap imageSelect;private Bitmap resizedPlanete;
    private int radius;
    private int screenW;
    private int screenH;
    private Context myContexte;
    private ArrayList<AstreCeleste> liste;
    private int w;
    private int h;
    public String couleur;
    public int type;
    public int taille;

    public planetes(Context mcontext, String nom, int taille, String couleur, int type, String nomImage, int height, int width) {
        super(mcontext);
        myContexte = mcontext;

        this.couleur = couleur;
        this.taille = taille;


        status = true;
        alea = new Random();

        posX = alea.nextInt(width -300);
        posY = alea.nextInt(height - 300);

        crayon = new Paint();
        crayon.setAntiAlias(true);

        int idImage= getResources().getIdentifier(nomImage,"drawable", getContext().getPackageName());
        imageSelect = BitmapFactory.decodeResource(getResources(),idImage);
        resizedPlanete = Bitmap.createScaledBitmap(imageSelect,taille ,taille,true);

        radius = (taille/2)+10;

    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
    }





    public boolean getStatus()
    {

        return this.status;
    }



    public void setStatus(Boolean state, String nom, String couleur, int taille, int type)
    {
        this.status = state;

        if (!this.status)
        {
            Toast.makeText(myContexte, "Nom : "+nom+"   Taille: "+taille+"", Toast.LENGTH_SHORT).show();

            if(type == 1)
            {
                crayon.setColor(Color.GREEN);

            }
        }
    }


    public int getPosX()
    {

        return this.posX;
    }

    public int getPosY()
    {
        return this.posY;
    }


    public void onDraw(Canvas canvas) {


        int x = getPosX();
        int y = getPosY();




        if(this.status)
        {
            crayon.setColor(Color.parseColor(couleur));
        }

        canvas.drawCircle(x, y, radius, crayon );

        canvas.drawBitmap(resizedPlanete, x-taille/2 , y-taille/2,null);
    }
}

